$(document).ready(function(){
    var question, option;
    var right = 0;
    var number = 0;
    var chapter = $('#chapter').val();
    
    loadQuestion(number);

    $(document).on('click', '.btn-option', function(){
        var status = $(this).attr('data-status');

        // Count quiz.
        if (status == 'true') {
            right++;
        }
        
        console.log('Right :' + right);

        // Load next.
        number++;
        loadQuestion(number);
    });

    function loadQuestion(number) {
        $('.exercise').hide().fadeIn(1000);
        $('.question').html('');
        $('.option').html('');

        $.ajax({
            method: "GET",
            url: 'content/exercise/' + chapter + '.json'
        })
        .fail(function () {
            $.notify('Gagal memuat latihan soal ..', 'error');
        })
        .done(function (data) {
            var question_number = number + 1;

            $('.indicator').html(`Pertanyaan <b>${question_number}</b> dari <b>${data.length}</b>`);

            if (question_number > data.length) 
            {
                // Count score
                let score;
                let sentence;
                let color;

                score = (right / data.length) * 100;
                
                if (score > 60) {
                    color = 'success';
                    sentence = `Selamat, nilai kamu ${score}!`;
                    $.notify(sentence, 'success');
                } else {
                    color = 'danger';
                    sentence = `Sayang sekali, nilai kamu cuma ${score}`;
                    $.notify(sentence, 'error');
                }

                $('.indicator').html('');
                
                $('.result').show().html(`
                    <div class="text-center">
                        <div class="mb-3"><b style="font-size:20px;">${sentence}</b></div>
                        <button class="btn btn-${color} btn-sm btn-try-again"><span class="fas fa-redo"></span>&nbsp;&nbsp;Latihan Lagi</button>
                    </div>
                `);
            } 
            else 
            {
                if (!data) {
                    $('.question').html('<p>Pertanyaan tidak dapat dibuat ..</p>');
                    $('.option').html('');
                } else {
                    $('.question').append(`
                        <div class="mb-4">
                            ${data[number].question}
                        </div>
                        `
                    );

                    $.each(data[number].option, function (key, value) {
                        option = `
                        <div class="custom-control custom-radio mt-2">
                            <input type="radio" id="${key}" name="soal" class="custom-control-input btn-option" data-status="${value.status}"/> 
                            <label class="custom-control-label" for="${key}"">${value.text}</label>
                        </div>`;

                        $('.option').append(option);
                    });
                }
            }
        });
        
        return false;
    }

    $(document).on('click', '.btn-try-again', function(){
        location.reload();
    });
});