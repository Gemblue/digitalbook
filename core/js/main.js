// General
let mode = 'twoPage';
let arrowControl = true;
let selectedPage;
let index = parseInt(getParameterByName('index'));
let indexLeft = 0;
let indexRight = 0;
let pinNavbar = true;
let bookFontSize;
let flipsound = new Audio('core/sound/page-flip-01a.mp3');
let showContextMenu = true;

// DB
let stabilloDB = new PouchDB('stabilloDB ' + bookTitle);
let notesDB = new PouchDB('notesDB ' + bookTitle);
let bookmarkDB = new PouchDB('bookmarkDB ' + bookTitle);
let remoteCouch = false;

// Stabillo
let stabilloMode = false;
let selectedText;
let stabilloChoice;

// Zoom
let zoomMode = false;

// Notes
let noteID;
let noteRev;
let noteColor = 'one';
let notePage = 'pageRight';
let notePosLeft = '46.1875';
let notePosTop = '92.59375';
let notesPrintableLeft = '';
let notesPrintableRight = ''; 
let showAllNote = false;

// Print
let printNotes = false;
let printStabillo = false;

// Table of contents
let showTableOfContents = false;

// Start.
const $book = $('#book');
const $body = $('body');

const background = {
  light: '#ddd',
  dark: '#333',
  image: `url("${$('.background-image .img-thumbnail').attr('src')}")`,
};

/** 
 * Init Machine
 */

// Load TOC
loadTOC();

// Set navbar
let settedPinNavbar = localStorage.getItem('pinNavbar');

if (settedPinNavbar == 'true') {
  pinNavbar = true;
  $('.btn-pin-navbar').html('<i class="fas fa-toggle-on"></i>');
} else {
  pinNavbar = false;
  $('.btn-pin-navbar').html('<i class="fas fa-toggle-off"></i>');
}

// Set book mode.
let settedMode = localStorage.getItem('mode');

if (settedMode == 'onePage') {
  mode = 'onePage';
  $book.removeClass('book--two-page');
  $('.book__arrow-right-onepage').removeClass('sr-only');
  $('input[name="layout"]').prop('checked', true);
} else {
  mode = 'twoPage';
  $book.addClass('book--two-page');
  $('.book__arrow-right-onepage').addClass('sr-only');
  $('input[name="layout"]').prop('checked', false);
}

// Set book font
bookFontSize = localStorage.getItem('bookFontSize');

if (!bookFontSize) { 
  bookFontSize = 15;
}

$('.book__body').css( 'font-size',  bookFontSize + 'px');

// Control index.
if (index <= 0 || index > maxIndex) {
  loadPage(0);
} else {
	loadPage(index);
}

/**
 * Navbar
 */
let navHovered = false;
setTimeout(function(){
  if(!navHovered) {
    if (pinNavbar == false) {
      $('.navbar-hover').addClass('hiding');
      $('.tooltip-target').tooltip('disable');
    }
  }
}, 3000);
$('.navbar-hover').on('mouseover', function(){
  navHovered = true;
  if (pinNavbar == false) {
    $(this).removeClass('hiding');
    $('.tooltip-target').tooltip('enable');
  }
}).on('mouseout', function(){
  navHovered = false;
  if (pinNavbar == false) {
    $(this).addClass('hiding');
    $('.tooltip-target').tooltip('disable');
  }
})

$(document).ready(function () {

  /**
   * Enable Tooltip
   */
  $('[data-tooltip="tooltip"]').tooltip(
    { boundary: 'viewport', delay: { "show": 300, "hide": 10 }, template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>' }
  )

  // Show page corner fold when page is hovered and blurred
  $('.book__page').on('mouseover', function(){
    let side = $(this).data('side');
    $('.page-nav__'+side).addClass('open-small');
    $('.book__arrow-'+side).addClass('show');
    $('.book__arrow-'+side+'-onepage').addClass('show');
  })
  .on('mouseout', function(){
    let side = $(this).data('side');
    $('.page-nav__'+side).removeClass('open-small');
    $('.book__arrow-'+side).removeClass('show');
    $('.book__arrow-'+side+'-onepage').removeClass('show');
  })

  // Init background options
  let bgtype = localStorage.getItem('bgtype');
  if(bgtype == null) bgtype = 'light';
  $('input#background-'+bgtype).prop('checked', true);
  $('#background-image-list').toggle(bgtype === 'image');
  updateBackground(bgtype);

  // Init stabillo options
  stabilloChoice = localStorage.getItem('stabilloChoice');
  if(stabilloChoice == null) stabilloChoice = 'one';
  $('input#stabillo-'+stabilloChoice).prop('checked', true);

  // Init print options
  printNotes = localStorage.getItem('printNotes');

  if (printNotes == 'checked')
    $('#printNotes').prop('checked', true);
  else
    $('#printNotes').prop('checked', false);

  printStabillo = localStorage.getItem('printStabillo');

  if (printStabillo == 'checked')
    $('#printStabillo').prop('checked', true);
  else
    $('#printStabillo').prop('checked', false);
})
// END

/**
 * Custom context menu
 * https://jsfiddle.net/enigmarm/b98ty/
 */
$(document).bind("contextmenu", function (event) {
  let top = event.pageY;
  let left = event.pageX;
  
  event.preventDefault();
  
  // Deteksi posisi, jika klik kanan agak kebawah, usahakan context menunya agak keatas biar gak ketutup.
  if (top > 439) {
    top = 439;
  }
  
  $('.context-menu').toggle(100).css({
    top: top + "px",
    left: left + "px"
  });

  $('.cm-stabillo, .cm-share, .cm-copy, .cm-download').show();
});

$('.context-menu').draggable();

$(document).on("click", "body", function () {
  
  // Hide sidebar
  $('.sidebar').hide();
  
  // Reset note all
  showAllNote = false;
  $('.btn-show-all-note').removeClass('active');
  
  // Reset table of contents
  showTableOfContents = false;
  $('.btn-show-table-of-contents').removeClass('active');
});
// END

/**
 * DOM Events
 */
$(document).ready(function () {
  
  /**
   * Hide preload
   */
  $('#preload').delay(1000).fadeOut();
  
  /**
   * General
   */
  $(window).resize(function(){
    if ($(window).width() <= 1160) {
      localStorage.setItem('mode', 'onePage');
      location.reload();
    }

    return false;
  });

  $('.btn-pin-navbar').click(function(){
    if (pinNavbar == false) {
      pinNavbar = true;
      localStorage.setItem('pinNavbar', 'true');
      $(this).html('<i class="fas fa-toggle-on"></i>');
    } else {
      pinNavbar = false;
      localStorage.setItem('pinNavbar', 'false');
      $(this).html('<i class="fas fa-toggle-off"></i>');
    }
  });
  // END
  
  /**
   * Feature : Settings menu
   */
  $('[data-toggle="offcanvas"]').click(function (e) {
    e.preventDefault();

    const targetEl = $(this).data('target');
    $(targetEl).toggleClass('is-active');
  });

  $('input[name="layout"]').change(function () {
    if ($(this).prop("checked") == true){
      localStorage.setItem('mode', 'onePage');
    } else {
      localStorage.setItem('mode', 'twoPage');
    }
    
    location.reload();
  });

  $('input[name="background"]').change(function () {
    const type = $('input[name="background"]:checked').val();

    $('#background-image-list').toggle(type === 'image');

    localStorage.setItem('bgtype', type);
    updateBackground(type);
  });

  $('input[name="stabillo"]').change(function () {
    stabilloChoice = $('input[name="stabillo"]:checked').val();
    localStorage.setItem('stabilloChoice', stabilloChoice);
    loadStabillo();
  });

  $('.background-image__item').click(function (e) {
    e.preventDefault();

    $('.background-image__item.is-active').removeClass('is-active');
    $(this).addClass('is-active');

    localStorage.setItem('bgtype', 'image');
    localStorage.setItem('bgid', $(this).attr('id'));
    updateBackground('image');
  });
  // END

  /**
   * Feature : Navigation
   */
  $('.js-next-page').click((e) => {
    turnPageleft();

    return false;
  });

  $('.js-prev-page').click((e) => {
    turnPageRight();

    return false;
  });

  $(document).keydown(function(e) {
    if (arrowControl == true) {
      switch(e.which) {
          case 39: // Left
          turnPageleft();
          break;

          case 37: // Right
          turnPageRight();
          break;

          default: return;
      }
      e.preventDefault(); // Prevent the default action (scroll / move caret)
    }
  });

  $('.btn-close-sidebar').click(function(){
    showTableOfContents = false;
    $('.btn-show-table-of-contents').removeClass('active');
    $('.sidebar').hide();
  });
  // END

  /**
   * Feature : Change Font Size
   */
  $('.btn-font-size').click(function(){
    let bookFontSize = localStorage.getItem('bookFontSize');
    
    bookFontSize++;

    if (bookFontSize > 20) {
      // Back to default
      bookFontSize = 15;
    }

    localStorage.setItem('bookFontSize', bookFontSize);

    $('.book__body').css( 'font-size', bookFontSize + 'px');
    $('.container').css( 'font-size', bookFontSize + 'px');
    $('#book__content__left').css( 'font-size', bookFontSize + 'px');
    $('#book__content__right').css( 'font-size', bookFontSize + 'px');
    
    return false;
  });
  // END

  /**
   * Feature : Jump to page.
   */
  $('.btn-jump').click(function(){
    arrowControl = false;
    $('#jumpModal').modal('show');

    return false;
  });

  $('.input-page').keypress(function (e) {
    if (e.which == 13) 
    {
      let inputPage = $(this).val();
      
      if (inputPage <= 0 || inputPage > maxPage) 
      {
        $.notify('Halaman tidak ditemukan.','error');
      } 
      else
      {
        arrowControl = true;
        
        index = pageReference.findIndex(function(item){ return item.file == inputPage + '.html'});
        loadPage(index);
        
        $('#jumpModal').modal('hide');
        
        $('.input-page').val('');
        
        $('#flip-next').addClass('flip-next-enter-active').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', () => {
          $('#flip-next').removeClass('flip-next-enter-active');
        });
        
        flipsound.play();
      }
      
      return false;
    }
  });
  // END

  /**
   * Feature : Searching .. 
   */
  $('.btn-open-search').click(function(){
    arrowControl = false;
    $('#searchModal').modal('show');
  });

  $('.input-search').keypress(function (e) {
    if (e.which == 13) {
        search();
    }
  });
  
  $('.btn-search').click(function(){
    search();
  });

  $(document).on("click", ".btn-goto-search", function () {
    let page = $(this).attr('data-page');
    index = pageReference.findIndex(function(item){ return item.file == page + '.html'});
    loadPage(index);
    arrowControl = true;
    $('#searchModal').modal('hide');
    $('.sidebar').hide();
    $('#flip-next')
      .addClass('flip-next-enter-active')
      .one('webkitAnimationEnd oanimationend msAnimationEnd animationend', () => {
        $('#flip-next').removeClass('flip-next-enter-active');
      });
    
    flipsound.play();

    return false;
  });
  // END

  /**
   * Feature : Bookmark
   */
  $('.btn-bookmark').click(function(){
    $('#bookmarkModal').modal('show');
    $('.bookmark-list').html('');

    bookmarkDB.allDocs({ include_docs: true, descending: true }, function (err, doc) {
        
        if (isEmpty(doc.rows)) {
          $('.bookmark-list').html('Belum ada ..');
        } else {
          $.each(doc.rows, function (index, value) {
              
              let output = `
                  <div class="item">
                    <div class="row">
                      <div class="col">
                        <a href="#" class="btn-table-contents" data-page="${value.doc.page}">Halaman ${value.doc.page}</a>
                      </div>
                      <div class="col">
                        <div class="text-right">
                          <a href="#" class="btn-remove-bookmark" data-rev="${value.doc._rev}" data-id="${value.doc._id}"><span class="far fa-trash-alt fa-fw"></span></a>
                        </div>
                      </div>
                    </div>
                  </div>
              `;

              $('.bookmark-list').append(output);
          });
        }
    });
  });
  
  $(document).on("click", ".btn-remove-bookmark", function () {
    let id = $(this).attr('data-id');
    let rev = $(this).attr('data-rev');
    
    bookmarkDB.remove(id, rev, null, function(){
        $('#bookmarkModal').modal('hide');
        $.notify('Berhasil dihapus.','success');
    });
  });

  $('.btn-save-bookmark').click(function () {
    
    if (mode == 'twoPage') 
    {
      bookmarkDB.post({ page: parseInt(pageReference[indexLeft].title) }, function callback(err, result) {
        if (!err) {
          bookmarkDB.post({ page: parseInt(pageReference[indexRight].title) }, function callback(err, result) {
            if (!err) {
              $.notify('Berhasil menyimpan halaman ' + pageReference[indexLeft].title + ' dan ' + pageReference[indexRight].title,'success');
            } else {
              alert('Failed to save ..');
            }
          });
        } else {
            alert('Failed to save ..');
        }
      });
    } 
    else 
    {
      bookmarkDB.post({ page: parseInt(pageReference[index].title) }, function callback(err, result) {
        if (!err) {
            $.notify('Berhasil menyimpan halaman ' + pageReference[index].title,'success');
        } else {
            alert('Failed to save ..');
        }
      });
    }
  });
  // END
  
  /**
   * Feature : Notes
   */
  $('.btn-note').click(function(){
    arrowControl = false;
    $('.context-menu').hide();
    $('.notes').show();
  });

  $(document).on("click", ".btn-cancel-note", function (event) {
    arrowControl = false;
    $('.notes').hide();
    $('.notes-detail').hide();
  });

  $(document).on("click", ".btn-show-all-note", function (event) {
    
    $('.all-note').html('');
    
    if (showAllNote == false) {
      showAllNote = true;
      $('.btn-show-all-note').addClass('active');
      
      // Get all notes from DB.
      notesDB.allDocs({ include_docs: true, descending: true }, function (err, doc) {
        
        if (!isEmpty(doc.rows)) {
          $.each(doc.rows, function (index, value) {
            $('.all-note').append(`
              <div class="item btn-goto-search" data-page="${value.doc.page}" data-id="${value.doc._id}" data-color="${value.doc.color}" data-rev="${value.doc._rev}">
                <div>${value.doc.content}</div>
                <div class="mt-2">
                  <div class="row">
                    <div class="col">
                      <small>Hal ${value.doc.page}</small>
                    </div>
                    <div class="col">
                      <div class="text-right">
                        <span class="btn-remove-note fas fa-trash-alt fa-fw" data-id="${value.doc._id}" data-rev="${value.doc._rev}"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            `);
          });
        } else {
          $('.all-note').html('Belum ada ..');
        }
        
        return true;
      });

      $('.sidebar-notes').show();
    } else {
      showAllNote = false;
      $('.btn-show-all-note').removeClass('active');
      $('.sidebar').hide();
    }

    return false;
  });

  $(document).on("click", ".btn-open-note", function (event) {
    noteID = $(this).attr('data-id');
    noteRev = $(this).attr('data-rev');
    
    notesDB.get(noteID).then(function (doc) {
      $('.note-content').val(doc.content);
      $('.notes-detail').toggle(100).
      css({
          top: event.pageY + "px",
          left: event.pageX + "px"
      });
    });

    return false;
  });

  $(document).on("click", ".btn-remove-note", function () {
    
    // Remove by attr.
    let id = $(this).attr('data-id');
    let rev = $(this).attr('data-rev');

    if (id == null && rev == null) {
      // Handle tombol hapus dari note di kertas.
      notesDB.remove(noteID, noteRev, null, function () {
        $('.notes-detail').hide();
        loadPage(page);
      });

      location.reload();

      return;
    }
    
    notesDB.remove(id, rev, null, function () {
      $('.all-note').html('');
      notesDB.allDocs({ include_docs: true, descending: true }, function (err, doc) {
        $.each(doc.rows, function (index, value) {
          $('.all-note').append(`
            <div class="item btn-goto-search" data-page="${value.doc.page}" data-id="${value.doc._id}" data-color="${value.doc.color}" data-rev="${value.doc._rev}">
              <div>${value.doc.content}</div>
              <div class="mt-2">
                <div class="row">
                  <div class="col">
                    <small>Hal ${value.doc.page}</small>
                  </div>
                  <div class="col">
                    <div class="text-right">
                      <a href="#" class="btn-remove-note" data-id="${value.doc._id}" data-rev="${value.doc._rev}"><span class="fas fa-trash-alt"></span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          `);
        });
        
        return true;
      });
    });
    
    return false;
  });

  $('.notes').draggable();
  
  $('.book__content__left').droppable({
    hoverClass: 'hover-droppable',
    drop: function( event, ui ) {
      notePage = 'pageLeft';      
    }
  });
  
  $('.book__content__right').droppable({
    hoverClass: 'hover-droppable',
    drop: function( event, ui ) {
      notePage = 'pageRight';
    }
  });
  
  $('.btn-save-note').click(function(){
    let page;
    let content = $('.input-note').val();
    
    if (notePage == 'pageLeft') {
      page = pageReference[indexLeft].title;
    } else {
      page = pageReference[indexRight].title;
		}
		
    // Save to DB
    let param = {
      _id: new Date().toISOString(),
      page: page,
      notePage: notePage,
      color: noteColor,
      content: content,
      positionLeft: notePosLeft,
      positionTop: notePosTop
    };

    notesDB.put(param, function callback(err, result) {
      if (!err) {
        arrowControl = true;
        $('.input-note').html('');
        $('.notes').hide();
        location.reload();
      } else {
        alert('Failed to save ..');
      }
    });
    
    return false;
  });

  $('.btn-edit-note').click(function(){
    let content = $('.note-content').val();
    
    // Update contentnya, kalau mekanisme indexedDB dia put konten baru dengan id yang sama.
    notesDB.get(noteID).then(function(doc) {
      notesDB.put({
        _id: doc._id,
        _rev: doc._rev,
        page: doc.page,
        notePage: doc.notePage,
        color: doc.color,
        content: content,
        positionLeft: doc.positionLeft,
        positionTop: doc.positionTop
      }).catch(function (err) {
        alert(err);
      });
    }).then(function(response) {
      $('.notes-detail').hide();
    }).catch(function (err) {
      alert(err);
    });

    return false;
  });

  $('.btn-note-color').click(function(){
    noteColor = $(this).attr('data-color');

    $('.btn-note-color').removeClass('active');
    $(this).addClass('active');

    return false;
  });
  // END

  /**
   * Feature : Print
   */
  $('#printButton').click(function () {
    window.print();
  });

  $('#printNotes').change(function () {
    if ($(this).is(":checked")) {
      localStorage.setItem('printNotes', 'checked');
    } else {
      localStorage.setItem('printNotes', '');
    }

    loadNotes('.notes-result-container-left', indexLeft);
    loadNotes('.notes-result-container-right', indexRight);
    
    return false;
  });

  $('#printStabillo').change(function () {
    if ($(this).is(":checked")) {
      localStorage.setItem('printStabillo', 'checked');
    } else {
      localStorage.setItem('printStabillo', '');
    }
    
    loadNotes('.notes-result-container-left', indexLeft);
    loadNotes('.notes-result-container-right', indexRight);
    
    return false;
  });
  
  /**
   * Feature : Stabillo
   */
  $('.btn-stabillo').click(function(){
    
    // Hide context menu
    $('.context-menu').hide();

    if (stabilloMode == false) {
      stabilloMode = true;
      // Icons made by Freepik/Flaticon
      $('.book__content').css( 'cursor', 'url("core/img/icon/stabillo.png"), auto' );
      $.notify('Fitur penanda diaktifkan.','success');
      $('.btn-stabillo').addClass('active');  
    } else {
      stabilloMode = false;
      $('.book__content').css( 'cursor', 'default' );
      $.notify('Fitur penanda dimatikan.', 'info');
      $('.btn-stabillo').removeClass('active'); 
    }
  });

  $('.book__content__left').on("mouseup", function (event) {
      if (stabilloMode == true) {
        
        $('.btn-stabillo').addClass('active');
        
        if (window.getSelection) {
          selectedText = window.getSelection().toString();
        } else if (document.selection) {
          selectedText = document.selection.createRange().text;
        }
        
        if (selectedText != '') {
          
          stabilloDB.put({
            _id: new Date().toISOString(),
            page: pageReference[indexLeft].title,
            selectedText: selectedText
          }, function callback(err, result) {
            if (!err) {
              loadStabillo();
            } else {
              alert('Failed to save ..');
            }
          });
        }
      }

      return false;
  });

  $('.book__content__right').on("mouseup", function (event) {
      if (stabilloMode == true) {  
        
        $('.btn-stabillo').addClass('active');

        if (window.getSelection) {
          selectedText = window.getSelection().toString();
        } else if (document.selection) {
          selectedText = document.selection.createRange().text;
        }
        
        if (selectedText != '') {
          
          stabilloDB.put({
            _id: new Date().toISOString(),
            page: pageReference[indexRight].title,
            selectedText: selectedText
          }, function callback(err, result) {
            if (!err) {
              loadStabillo();
            } else {
              alert('Failed to save ..');
            }
          });
        }
      }

      return false;
  });

  $(document).on("click", ".btn-remove-selected", function (event) {
    removeStabillo(pageReference[selectedPage].title);
    
    return false;
  });
  
  $(document).on("mousedown", ".book__content__right mark", function (event) {
    if (event.which == 3) {
      selectedPage = indexRight;
      $('.cm').hide();
      $('.cm-remove-stabillo').show();
    }
  });
  $(document).on("mousedown", ".book__content__left mark", function (event) {
    if (event.which == 3) {
      selectedPage = indexLeft;
      $('.cm').hide();
      $('.cm-remove-stabillo').show();
    }
  });
  // END

  /**
   * Feature : Multimedia
   */
  /*
  $(document).on("click", ".btn-play-video", function () {
    let video = $(this).attr('data-video');
    let output = `
        <video style="width:100%;" height="240" controls>
            <source src="assets/videos/${video}" type="video/mp4">
            Your browser does not support the video tag.
        </video>
    `;
    
    $('#videosModal').modal('show');
    $('.video-output').html(output)

    return false;
  }); */
  // END

  /**
   * Feature : Home
   */
  $(document).on("click", ".btn-home", function () {
    window.location = "index.html";
    return false;
  });
  // END

  /**
   * Feature : Table of contents
   */
  $('.btn-show-table-of-contents').click(function(){
    if (showTableOfContents == false) {
      showTableOfContents = true;
      $('.btn-show-table-of-contents').addClass('active');
      $('.sidebar-table-of-contents').show();
    } else {
      showTableOfContents = false;
      $('.btn-show-table-of-contents').removeClass('active');
      $('.sidebar').hide();
    }

    return false;
  }); 

  $(document).on('click', '.btn-table-contents', function () {
    showTableOfContents = false;
    
    // Hide everything
    $('.sidebar').hide();
    $('#bookmarkModal').modal('hide');
    
    // Trigger animation.
    $('#flip-next')
      .addClass('flip-next-enter-active')
      .one('webkitAnimationEnd oanimationend msAnimationEnd animationend', () => {
        $('#flip-next').removeClass('flip-next-enter-active');
      });
    
    // Sound
    flipsound.play();

    // Load page.
    let page = $(this).attr('data-page');
    index = pageReference.findIndex(function(item){ return item.file == page + '.html'});
    loadPage(index);
    
    return false;
  });
  // END

  /**
   * Feature : Simulation
   */
  $(document).on('click', '.btn-simulation', function () {

    let url = $(this).attr('data-url');
    let content = `
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="${url}" allowfullscreen></iframe>
      </div>`;
    
    $('.simulation-content').html(content);

    $('#simulationModal').modal('toggle');

    return false;
  });

  /**
   * Feature : Zoom
   */
  $('.btn-zoom').click(function(){

    // Hide context menu
    $('.context-menu').hide();

    if (zoomMode == false) {
      $.notify('Fitur perbesar diaktifkan.','success');
      zoomMode = true;
      $(this).addClass('active');
      // Icons made by Freepik/Flaticon
      $('.book__content').css( 'cursor', 'url("assets/img/zoom.png"), auto' );
    } else {
      $.notify('Fitur perbesar dimatikan.','info');
      zoomMode = false;
      $(this).removeClass('active');
      $('.book__content').css( 'cursor', 'default' );
    }
  });

  let zoomOn = false;
  
  document.querySelector( '.book__content__left' ).addEventListener( 'click', function( event ) {
    if (zoomMode == true) {
      
      if (zoomOn == false) {
        zoomOn = true;
        $('.offcanvas').hide();
        event.preventDefault();
        zoom.to({ element: event.target });
      } else {
        zoomOn = false;
        $('.offcanvas').show();
        event.preventDefault();
        zoom.out();
      }
    }
  });
  
  document.querySelector( '.book__content__right' ).addEventListener( 'click', function( event ) {
    if (zoomMode == true) {
      event.preventDefault();
      zoom.to({ element: event.target });
    }
  });
  // END

  /**
   * Help Feature
   */
  $('.btn-help').click(function(){
    $('#helpModal').modal('show');
    $('[data-toggle="offcanvas"]').trigger('click')
  });

  /**
   * Feature : Half Download / Share
   */
  $('.book__content').on("mouseup", function (event) {
    
    // Hanya aktif jika stabillo mati..
    if (stabilloMode == false)
    {
    
      if (window.getSelection) {
        selectedText = window.getSelection().toString();
      } else if (document.selection) {
        selectedText = document.selection.createRange().text;
      }

      // Show context menu
      if (selectedText.length >= 10) {
        // Put to textarea share modal
        $('#selectedText').html('Kutipan ' + bookTitle + ': '+ selectedText);
        
        // Put to notes content.
        $('.input-note').html(selectedText);

        // Disable
        $('#selectedText').attr('readonly', 'readonly');
        
        // Show context menu
        $('.cm').hide();
        $('.cm-stabillo, .cm-share, .cm-copy, .cm-download').show();
        
        // Show context menu.
        let top = event.pageY;
        let left = event.pageX;

        // Deteksi posisi, jika klik kanan agak kebawah, usahakan context menunya agak keatas biar gak ketutup.
        if (top > 439) {
          top = 439;
        }
        
        $('.context-menu').toggle(100).
        css({
            top: top + "px",
            left: left + "px"
        });
      }
    }
    
  });

  /**
   * Feature : Share
   */
  $('.btn-share').click(function(){
    $('#shareModal').modal('show');
    $('#selectedText').html(bookTitle + ' ' + 'https://buku.kemdikbud.go.id/buku/ipa-semester-2.html');
    $('#selectedText').removeAttr('readonly'); 
  });

  $('.btn-share-facebook').click(function(){
    let text = $('#selectedText').val();
    let url = 'https://www.facebook.com/sharer/sharer.php?u=http://buku.kemdikbud.go.id&quote=' + text;
    window.open(url);
  });

  $('.btn-share-twitter').click(function(){
    let text = $('#selectedText').val();
    let url = 'https://twitter.com/intent/tweet?text=' + text;
    window.open(url);
  });

  $('.btn-share-whatsapp').click(function(){
    let text = $('#selectedText').val();
    let url = 'whatsapp://send?text=' + text;
    window.open(url);
  });
  // END

  /**
   * Feature : Context Menu
   */
  $('.btn-context-menu-close').click(function(){

    // Normalize content
    $('.book__content').css( 'cursor', 'default' );
    
    // Turn off zoom
    zoomMode = false;
    $('.btn-zoom').removeClass('active');

    // Turn off stabillo
    stabilloMode = false;
    $('.btn-stabillo').removeClass('active');

    // Turn off note
    $('.notes').hide();
    $('.input-note').html('');
    
    // Clean context menu
    $('.cm').hide();
    $('.cm-stabillo, .cm-share, .cm-download').show();
    $('.context-menu').hide();
  });

  $('.cm-share').click(function(){
    $('#shareModal').modal('show');
    $('.context-menu').hide();
  });

  $('.cm-copy').click(function(){
    document.execCommand("copy");
    $('.context-menu').hide();
    $.notify('Tersalin.','success');

    return false;
  })

  $('.cm-download').click(function(){
    let filename = 'Buku@' + Math.random() + '.txt';
    let blob = new Blob([selectedText], {
      type: "text/plain;charset=utf-8"
    });
    
    // Save
    saveAs(blob, filename);
    
    // Clean
    $('.btn-context-menu-close').trigger('click');

    return false;
  });
  // END
});

/**
 * Helper
 */
function updateBackground(type) {
  let bgid = localStorage.getItem('bgid');
  if(bgid === null) bgid = 'bg-1';

  $('.background-image__item').removeClass('is-active');
  $('#'+bgid).addClass('is-active');

  background.image = `url("${$('.background-image__item.is-active .img-thumbnail').attr('src')}")`;

  // Set attribution
  if($('#'+bgid).data('attribution') == ''){
    $('.background-attribution').addClass('sr-only');
  } else {
    $('.background-attribution').removeClass('sr-only');
    $('.background-attribution a span').html($('#'+bgid).data('attribution'));
    $('.background-attribution a').attr('href', $('#'+bgid).data('attribution-url'));
  }

  if (type == 'dark') {
    $('body').addClass('dark');
    $('.nav').addClass('dark');
    $('.nav-link').addClass('dark');
    $('.book__page').addClass('dark');
  } else {
    $('body').removeClass('dark');
    $('.nav').removeClass('dark');
    $('.nav-link').removeClass('dark');
    $('.book__page').removeClass('dark');
  }

  if (type == 'light') {
    $body.css({
      background: '#ffefba',
      background: '-webkit-linear-gradient(to right, #ffefba, #ffffff)',
      background: 'linear-gradient(to right, #ffefba, #ffffff)',
      backgroundSize: 'cover',
      backgroundAttachment: 'fixed',
      backgroundPosition: '50% 80%',
    });
    $('.book__arrow a').addClass('text-dark').removeClass('text-light');
    $('.bg-overlay').addClass('sr-only');
  } else if (type == 'dark') {
    $body.css({
      background: '#141e30',
      background: '-webkit-linear-gradient(to right, #141e30, #243b55)',
      background: 'linear-gradient(to right, #141e30, #243b55)',
      backgroundSize: 'cover',
      backgroundAttachment: 'fixed',
      backgroundPosition: '50% 80%',
    });
    $('.book__arrow a').addClass('text-light').removeClass('text-dark');
    $('.bg-overlay').addClass('sr-only');
  } else {
    $body.css({
      background: background[type],
      backgroundSize: 'cover',
      backgroundAttachment: 'fixed',
      backgroundPosition: '50% 80%',
    });
    $('.bg-overlay').removeClass('sr-only');
    $('.book__arrow a').addClass('text-light').removeClass('text-dark');
  }
}

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'), results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function loadPage(index) {
  
  // Reset anything
  $('.sidebar').hide();
  showAllNote = false;
  $('.btn-show-all-note').removeClass('active');
  showTableOfContents = false;
  $('.btn-show-table-of-contents').removeClass('active');
	
  // Prepare
  $('.book__content__left').html('');
  $('.book__content__right').html('');
  
  // Push to URL
  history.pushState(null, null, "?index=" + index);
  
  // TwoPage.
  if (mode == 'twoPage') 
  {
    indexLeft = index;
    indexRight = parseInt(indexLeft) + 1;
    
    $.ajax({
      method: "GET",
      
      url: 'content/' + pageReference[indexLeft].file
    })
    .fail(function () {
      $.notify('Halaman tidak ditemukan.','error');
    })
    .done(function (data) {

      $('.book__content__left').hide().html(data).fadeIn();
      $('.loading').hide();
      
      // Call MathJax
      if (typeof MathJax != "undefined") {
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'book__content__left']);
      }

      // Call Plyr
      const player = new Plyr('video');

      // Lanjut load halaman kanan.
      $.ajax({
        method: "GET",
        
        url: 'content/' + pageReference[indexRight].file
      })
      .fail(function () {
        $.notify('Halaman tidak ditemukan.','error');
      })
      .done(function (data) {
        $('.book__content__right').hide().html(data).fadeIn();
        
        // Call MathJax
        if (typeof MathJax != "undefined") {
          MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'book__content__right']);
        }
        // Call Plyr
        const player = new Plyr('video');

        $('.page-right').html(pageReference[indexRight].title);
        $('.page-left').html(pageReference[indexLeft].title);
        
        loadNotes('.notes-result-container-left', indexLeft);
        loadNotes('.notes-result-container-right', indexRight);
        loadStabillo();

        // Hide page button for page 1 and last page
        $('.page-nav__left').removeClass('sr-only');
        $('.book__arrow-left').removeClass('sr-only');
        $('.book__page--left').removeClass('first-page');
        if(indexLeft == 0){
          $('.page-nav__left').addClass('sr-only');
          $('.book__arrow-left').addClass('sr-only');
          $('.book__page--left').addClass('first-page');
        }

        $('.page-nav__right').removeClass('sr-only');
        $('.book__arrow-right').removeClass('sr-only');
        $('.book__page--right').removeClass('last-page');
        if(indexRight == maxIndex){
          $('.page-nav__right').addClass('sr-only');
          $('.book__arrow-right').addClass('sr-only');
          $('.book__page--right').addClass('last-page');
        }

      });
    });
  }

  // OnePage.
  else if (mode == 'onePage') 
  {
    $.ajax({
      method: "GET",
      url: 'content/' + pageReference[index].file
    })
    .fail(function () {
      $.notify('Halaman tidak ditemukan.','error');
    })
    .done(function (data) {
      $('.book__content__right').hide().html(data).fadeIn();
      $('.loading').hide();
      $('.page-right').html(pageReference[index].title);
      
      // Call MathJax
      if (typeof MathJax != "undefined") {
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'book__content__right']);
      }

      // Call Plyr
      const player = new Plyr('video');

      loadNotes('.notes-result-container-right', index);
      loadStabillo();
    });
  }
}

function search() {
  let input = $('.input-search').val();

  $.ajax({
    method: "GET",
    url: 'content/js/content.json'
  })
  .fail(function () {
    $.notify('Pencarian gagal.','error');
  })
  .done(function (data) {

    $('.search-list').html('');
    
    $.each(data, function (key, value) {
      let myReg = new RegExp(input + ".*");
      let myMatch = value.content.match(myReg);
      if (myMatch != null) {

        let content = value.content;
        content = content.replace(input, '<b><u>' + input + '</u></b>');

        let output = `
          <a class="btn-goto-search" href="#" data-page="${value.page}">
            <div class="item">
              Hal ${value.page}: ${content}
            </div>
          </a>`;

        $('.search-list').append(output);
      }
    });
  });
}

function isEmpty(obj) {
  for (let key in obj) {
      if (obj.hasOwnProperty(key))
          return false;
  }
  return true;
}

function ObjectLength( object ) {
  let length = 0;
  for( let key in object ) {
      if( object.hasOwnProperty(key) ) {
          ++length;
      }
  }
  return length;
};

function loadNotes(position, index) {

  let page = pageReference[index].title;  

  $('.notes-result-container').html('');

  notesPrintableLeft = '';
  notesPrintableRight = '';

  notesDB.allDocs({ include_docs: true, descending: true }, function (err, doc) {
    
    $.each(doc.rows, function (index, value) {
      if (value.doc.page == page) {
        let notes;
        
        notes = `
          <div class="d-print-none notes-result notes-${value.doc._id} ${value.doc.color}" data-id="${value.doc._id}" data-rev="${value.doc._rev}">
            <a href="#" class="btn-open-note" data-id="${value.doc._id}" data-rev="${value.doc._rev}">
              <span class="far fa-file-alt"></span>
            </a>
          </div>
        `;

        // Embed note result.
        $(position).append(notes);        
        
        // Embed note to page for print.
        if (localStorage.getItem('printNotes') == 'checked') {
          if (value.doc.content != null) {
            if (value.doc.notePage == 'pageLeft') {
              notesPrintableLeft = `<div class="embedded-notes card mb-3">
                                      <div class="card-body">
                                        <b>Catatan</b> : <br/>${value.doc.content}</div>
                                      </div>
                                     </div>`;

               $('.book__content__left').append(notesPrintableLeft);
            } else {                                                        
              notesPrintableRight = `<div class="embedded-notes card mb-3">
                                      <div class="card-body">
                                        <b>Catatan</b> : <br/>${value.doc.content}</div>
                                      </div>
                                     </div>`;
              $('.book__content__right').append(notesPrintableRight);
            }
          }
        } else {
          $('.embedded-notes').remove();
        }     
      }
    });

    return true;
  });
}

function loadTOC() {
  $.ajax({
    method: 'GET',
    url: 'content/toc.html'
  })
  .fail(function () {
    $.notify('Daftar isi tidak dapat dimuat, sepertinya belum dibuat.', 'error');
  })
  .done(function (data) {
    $('.table-of-contents').html(data);
  });
}

function loadStabillo() {    
  stabilloDB.allDocs({ include_docs: true, descending: true }, function (err, doc) {
    $.each(doc.rows, function (index, value) {
      if (value.doc.page == pageReference[indexLeft].title || value.doc.page == pageReference[indexRight].title) 
      {
        $('.book__content__left p, .book__content__right p').mark(value.doc.selectedText, {
          "accuracy": "complementary",
          "caseSensitive": true,
          "ignoreJoiners": true,
          "acrossElements": true,
          "separateWordSearch": false,
          "diacritics": false
        });

        if (localStorage.getItem('stabilloChoice') == 'one') {
          $('mark').css({
            'padding':'0px',
            'color': '#444',
            'background': '#2ecc71',
            'text-decoration': 'none',
            'position': 'relative'
          });
        } else if (localStorage.getItem('stabilloChoice') == 'two') {
          $('mark').css({
            'padding':'0px',
            'color': '#444',
            'background': 'orange',
            'text-decoration': 'none',
            'position': 'relative'
          });
        } else {
          $('mark').css({
            'padding':'0px',
            'color': '#444',
            'background': 'transparent',
            'text-decoration': 'underline',
            'position': 'relative'
          });
        }
      }
    });
    
  });

  return true;        
}

function removeStabillo(page)
{
  stabilloDB.allDocs({ include_docs: true, descending: true }, function (err, doc) {
    let length = ObjectLength(doc.rows);
    
    if (length > 0) {
      let i = 1;
      $.each(doc.rows, function (index, value) {
        if (value.doc.page == page) {
          stabilloDB.remove(value.doc._id, value.doc._rev, null);
        }
        
        if (i >= length) {
          $('.context-menu').hide();
          $('.cm-stabillo').show();
          $('.cm-remove-stabillo').hide();
          loadPage(parseInt(getParameterByName('index')));

          return false;
        }
        
        i++;
      });  
    }
  });

  return false;
}

function turnPageleft()
{
  let allowTurn = true;
	
  if (allowTurn == false) {
    $.notify('Halaman telah mencapai batas akhir.','error');
    return false;
  } else {

    $('#flip-next').addClass('flip-next-enter-active').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', () => {
      $('#flip-next').removeClass('flip-next-enter-active');
    });
    
    if (mode == 'twoPage') {
      index++;
      index++;
    } else {
      index++;
    }
		
    new Audio('core/sound/page-flip-01a.mp3').play();

    loadPage(index);  
  } 
}

function turnPageRight()
{
  let allowTurn = true;

  if (allowTurn == false) {
    $.notify('Halaman telah mencapai batas awal.','error');
    return false;
  } else {
    
    $('#flip-prev').addClass('flip-prev-enter-active').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', () => {
      $('#flip-prev').removeClass('flip-prev-enter-active');
    });
    
    if (mode == 'twoPage') {
      index--;
      index--;
    } else {
      index--;
    }
    
    new Audio('core/sound/page-flip-01a.mp3').play();

    loadPage(index);

  }
}