/*
.page-b is flipped page in left
.page-c is opened page in left
.page-d is opened page in right
.page-e is flipped page in right
.page-f is covered page in right
*/
let pageNum = 1;
let maxpage = 7;
let speed = 300;

$(function(){
  $('.page-container').each(function(index){
    new SimpleBar($(this)[0]);
  });

  $('.next').on('click', function(){
    let nextBtn = $(this);
    nextBtn.addClass('disabled').prop('disabled', true);

    if (pageNum >= maxpage){
      nextBtn.removeClass('disabled').prop('disabled', false);
      return;
    }

    let wrapper = $('.page-wrapper');
    let pageB = wrapper.children('.page-b');
    let pageC = wrapper.children('.page-c');
    let pageD = wrapper.children('.page-d');
    let pageE = wrapper.children('.page-e');
    let pageF = wrapper.children('.page-f');

    pageD.addClass('turn-left');

    // tunggu speed ms
    setTimeout(function(){
      pageD.removeClass('turn-left page-d').addClass('page-b');
      pageE.addClass('turn-left');
      
      // tunggu speed ms lagi
      setTimeout(function(){
        pageB.remove();
        pageC.remove();
        pageE.removeClass('turn-left page-e').addClass('page-c');
        pageF.removeClass('page-f').addClass('page-d');

        loadNextPage();
        if(parseInt(pageNum) == 1){
          alert('ee')
          pageE.addClass('first-page');
        }

        nextBtn.removeClass('disabled').prop('disabled', false);
      }, speed*2);
    }, speed);
  })

  $('.prev').on('click', function(){
    let prevBtn = $(this);
    prevBtn.addClass('disabled').prop('disabled', true);

    if (pageNum == 1){
      prevBtn.removeClass('disabled').prop('disabled', false);
      return;
    } 

    let wrapper = $('.page-wrapper');
    let pageB = wrapper.children('.page-b');
    let pageC = wrapper.children('.page-c');
    let pageD = wrapper.children('.page-d');
    let pageE = wrapper.children('.page-e');
    let pageF = wrapper.children('.page-f');

    loadPrevPage();

    pageC.addClass('turn-right');

    // tunggu speed ms
    setTimeout(function(){
      pageC.removeClass('turn-right page-c').addClass('page-e');
      pageB.addClass('turn-right');
      pageD.removeClass('page-d').addClass('page-f');
      pageE.remove();
      pageF.remove();
      
      // tunggu speed ms lagi
      setTimeout(function(){
        pageB.removeClass('turn-right page-b').addClass('page-d');

        prevBtn.removeClass('disabled').prop('disabled', false);
      }, speed*2);
    }, speed);
  })
})

function loadNextPage()
{
  if(pageNum < maxpage-3)
    $('.page-wrapper').append(`<div class="page page-e"><div class="page-container"><img src="assets/img/bab${pageNum+3}.png"></div></div>`);

  if(pageNum < maxpage-4)
    $('.page-wrapper').append(`<div class="page page-f"><div class="page-container"><img src="assets/img/bab${pageNum+4}.png"></div></div>`);

  pageNum = pageNum + 2;
  console.log(pageNum);

  $('.page-container').each(function(index){
    new SimpleBar($(this)[0]);
  });
}

function loadPrevPage()
{
  if(pageNum-4 > 0)
    $('.page-wrapper').prepend(`<div class="page page-b"><div class="page-container"><img src="assets/img/bab${pageNum-4}.png"></div></div>`);

  if(pageNum-3 > 0)
    $('.page-wrapper').prepend(`<div class="page page-c"><div class="page-container"><img src="assets/img/bab${pageNum-3}.png"></div></div>`);

  pageNum = pageNum - 2;
  console.log(pageNum);

  $('.page-container').each(function(index){
    new SimpleBar($(this)[0]);
  });
}