# Proyek Buku Elektronik

Client :

Pusat Perbukuan, Kemdikbud

Developer :

- Ahmad Oriza Sahputra S, Kom 
- Cahaya Arifin Hanafi S, Pd
- Toni Haryanto S, Pd
- Hendra Arfiansyah S, Pd

Feature :

- Download
- Notes
- Stabillo
- Quiz
- Jump
- Change Background
- Search
- Print
- Share
- Multimedia hyperlink, video, sound, game/simulation, quiz
- Table of Contents
- Bookmark.
- Zoom